﻿-- Listar os empregados que não têm dependentes
SELECT fnome, minicial, lnome
FROM empresa.empregado e
WHERE NOT EXISTS (
    SELECT *
    FROM empresa.dependente d
    WHERE e.ssn = d.essn);

-- Listar os empregados que trabalham somente para o projeto 'ProdutoX'
SELECT fnome, minicial, lnome
FROM empresa.empregado, empresa.trabalha_em, empresa.projeto
WHERE ssn = essn AND pno = pnumero AND pjnome = 'ProdutoX';

-- 8.14 a. Para cada departamento cuja média do salário dos empregados seja 
-- maior do que $30.000, recuperar o nome do departamento e o número de
-- empregados trabalhando para aquele departamento.

SELECT dnome, count(*)
FROM empresa.departamento d JOIN empresa.empregado e ON d.dnum = e.dno
GROUP BY d.dnome
HAVING AVG(e.salario) > 30000;

-- 8.14 b. Suponha que nós quiséssemos o número de empregados do sexo mascu-
-- lino ao invés de todos os empregados. Podemos especificar essa consulta em
-- SQL? Justifique.

SELECT d.dnome, count(*)
FROM empresa.departamento d, empresa.empregado e
WHERE e.sexo = 'M'
    AND d.dnum = e.dno 
    AND d.dnum IN (
            SELECT dnum
            FROM empresa.departamento d, empresa.empregado e
            WHERE d.dnum = e.dno
            GROUP BY d.dnum
            HAVING AVG(e.salario) > 30000
)
GROUP BY d.dnome;

-- Considerando que a média é do salário dos funcionários do sexo masculino, basta
-- acrescentar uma cláusula WHERE e.sexo = 'M' na consulta 8.14 a. Entretanto para 
-- mostrar o número de empregados do sexo masculino após a média geral, devemos 
-- aninhar a consulta conforme demonstrado acima.

-- Resolver o exercício 8.13 (somente e, f, g, h, i) do livro de Elmasri.
-- Especifique as consultas do exercício 6.16 em SQL.

-- 6.16 e. Recupere os nomes de todos os empregados que trabalhem em todos os projetos.

SELECT e.fnome, e.lnome
FROM empresa.empregado e, empresa.trabalha_em t, empresa.projeto p
WHERE e.ssn = t.essn AND t.pno = p.pnumero
GROUP BY e.fnome, e.lnome
HAVING COUNT(*) > (SELECT count(*)
		   FROM empresa.projeto);

-- 6.16 f. Recupere os nomes de todos os empregados que não trabalham em nenhum projeto.

SELECT fnome, lnome
FROM empresa.empregado e
WHERE NOT EXISTS (SELECT *
		  FROM empresa.trabalha_em t
		  WHERE e.ssn = t.essn);

-- 6.16 g. Para cada departamento, recupere o nome do departamento e a média salarial de
--         todos em empregados que trabalhem nesse departamento.

SELECT dnome, AVG(e.salario)
FROM empresa.departamento d join empresa.empregado e on d.dnum = e.dno
GROUP BY dnome;

-- 6.16 h. Recupere a média salarial de todos os empregados do sexo feminino.
SELECT AVG(e.salario)
FROM empresa.empregado e
WHERE e.sexo = 'F';

-- 6.16 j. Encontre os nomes e os endereços de todos os empregados que trabalhem em pelo
--         menos um projeto localizado em Houston, mas cujo departamento não se localiza
--         em Houston.
SELECT e.fnome, e.lnome, e.endereco
FROM empresa.empregado e, empresa.trabalha_em t, empresa.projeto p
WHERE p.plocalizacao LIKE 'Houston' 
  AND p.pnumero = t.pno 
  AND t.essn = e.ssn 
  AND EXISTS   (SELECT *
		FROM empresa.dept_localizacoes l
		WHERE l.dlocalizacao
		NOT LIKE 'Houston' 
		AND e.dno = l.dnum)
GROUP BY e.fnome, e.lnome, e.endereco;

-- Resolver o exercício 8.15 (somente d, e, g, h, j, k) do livro de Elmasri.
-- Especifique as atualizações do exercício 5.10 usando comandos de atualização SQL.
-- (Discuta todos as restrições de integridade violadas por cada operação, se houver alguma, e os
-- diferentes mecanismos para impor essas restrições.)

-- 5.10 d. Insira <'677678989',null,'40,0'> em trabalha_em.
INSERT INTO empresa.trabalha_em
VALUES ('677678989',null,40);
-- Valor nulo na coluna pno viola restrição de integridade referencial entre,
-- trabalha_em.pno e projeto.pnumero

-- 5.10 e. Insira <'453453453','John',M,'1970-12-12','CONJUGE'> em dependente.
INSERT INTO empresa.dependente
VALUES ('453453453','John', 'M','1970-12-12','CONJUGE');

-- 5.10 g. Remova a tupla de empregado com ssn='987654321'.
DELETE FROM empresa.empregado
WHERE ssn='987654321';
-- Viola restrição de chave estrangeira, pois '987654321' é superssn de 
-- outras tuplas. Para haver a remoção, o banco precisaria de
-- alterações em diversas restrições para CASCADE. Em Postgres
-- isso implica em remover as restrição e, usando ALTER TABLE, reincluí-las.
-- Consideremos as restrições definidas na página 154.

-- 5.10 h. Remova a tupla de projeto com pjnome = 'ProdutoX'.
DELETE FROM empresa.projeto
WHERE pjnome LIKE 'ProdutoX';

-- 5.10 j. Modifique o atributo superssn da tupla de empregado com ssn = '9998887777' para '943775543'.
UPDATE empresa.empregado
SET ssn = '943775543'
WHERE ssn = '999887777';

-- 5.10 k. Modifique o atributo horas da tupla de trabalha_em com essn = '9998887777' e pno = 10 para '5,0'.
UPDATE empresa.trabalha_em
SET horas = 5.0
WHERE essn = '999887777' AND pno = 10;
-- O ssn da tupla cujo ssn é '999887777' já havia sido alterado para '943775543', portanto não teve efeito.