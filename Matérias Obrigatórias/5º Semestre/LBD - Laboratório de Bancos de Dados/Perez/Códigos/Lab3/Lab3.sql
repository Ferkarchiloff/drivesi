﻿create view empresa.visaoA as 
select d.dnome, e.fnome, e.lnome,  e.salario
from empresa.departamento d, empresa.empregado e
where d.gerssn = e.ssn
group by d.dnome, e.fnome, e.lnome,  e.salario;

create view empresa.visaoB as
select e.fnome, e.lnome, s.lnome as snome, e.salario
from empresa.empregado e, empresa.empregado s, empresa.departamento d
where d.dnome = 'Pesquisa' AND e.dno = d.dnum AND e.superssn = s.ssn;


create table empresa.aux as 
select e.dno as dno, count(*) as c
from empresa.empregado e
group by e.dno;


create  table empresa.aux2 as 
select t.pno as pno, sum (t.horas) as s
from empresa.trabalha_em t, empresa.empregado e
where t.essn = e.ssn
group by t.pno;

create view empresa.visaoC as
select p.pjnome, d.dnome, a.c, a2.s
from empresa.projeto p, empresa.departamento d, empresa.aux a, empresa.aux2 a2
where p.dnum = d.dnum AND d.dnum = a.dno AND p.pnumero = a2.pno;


